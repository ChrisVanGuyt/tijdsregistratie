﻿using System;
using System.IO;
using System.Xml.Serialization;
using Tijdsregistratie.Models;
using Tijdsregistratie.Models.DabaseModel;

namespace Tijdsregistratie.Controls
{
    class DatabaseControl
    {
        public static TimeRegistrationModel Read(string path)
        {
            if (string.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentException($"'{nameof(path)}' cannot be null or whitespace", nameof(path));
            }

            using (Stream reader = new FileStream(path, FileMode.Open))
            {
                return Read(reader);
            }
        }

        private static TimeRegistrationModel Read(Stream reader)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(timeregistration));
            timeregistration t = (timeregistration)serializer.Deserialize(reader);
            return (TimeRegistrationModel)t;
        }

        public static void Write(TimeRegistrationModel value, string path)
        {
            if (value is null)
            {
                throw new ArgumentNullException(nameof(value));
            }

            if (string.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentException($"'{nameof(path)}' cannot be null or whitespace", nameof(path));
            }

            using (StreamWriter writer = new StreamWriter(path))
            {
                timeregistration timeregistration = (timeregistration)value;
                XmlSerializer serializer = new XmlSerializer(typeof(timeregistration));
                serializer.Serialize(writer, timeregistration);
            }
        }
    }
}
