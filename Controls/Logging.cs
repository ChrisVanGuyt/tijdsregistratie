﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Globalization;
using System.Reflection;

namespace Tijdsregistratie.Controls
{
    public class Logging
    {
        public static void AddDataToException(Exception ex, string key, bool value) { AddDataToException(ex, key, value ? "true" : "false"); }
        public static void AddDataToException(Exception ex, string key, int value) { AddDataToException(ex, key, value.ToString()); }

        public static void AddDataToException(Exception ex, string key, string value)
        {
            if (ex.Data.Count >= 100)
            {
                return;
            }

            for (int index = 0; index <= 100; index++)
            {
                string k2 = key + (index == 0 ? "" : index.ToString());

                if (!ex.Data.Contains(k2))
                {
                    ex.Data.Add(k2, string.IsNullOrEmpty(value) ? value == null ? "null" : "empty" : value);
                    return;
                }
            }
        }

        public static string ExceptionToMessage(Exception exception)
        {
            string message = "";

            for (Exception e = exception; e != null; e = e.InnerException)
            {
                if (!string.IsNullOrEmpty(message)) message += "\n\n\nINNER EXCEPTION ";
                message += e.Message + "\n";
                if (!string.IsNullOrEmpty(e.Source)) message += $"\nException caused by {e.Source}";

                StackTrace stackTrace = new StackTrace(e, true);

                if (stackTrace != null)
                {
                    message += "\n\nStack trace\n";

                    for (int index = 0; index < stackTrace.FrameCount; index++)
                    {
                        StackFrame stackFrame = stackTrace.GetFrame(index);
                        message += "\n   ";

                        if (!string.IsNullOrEmpty(stackFrame.GetMethod().Name)) message += $" at {stackFrame.GetMethod().Name}";

                        message += "(";

                        foreach (ParameterInfo parameterInfo in stackFrame.GetMethod().GetParameters())
                        {
                            if (!message.EndsWith("(")) message += ", ";
                            message += parameterInfo.Name;
                        }

                        message += ")";

                        if (!string.IsNullOrEmpty(stackFrame.GetFileName())) message += $" in {stackFrame.GetFileName()}";
                        if (stackFrame.GetFileLineNumber() != 0) message += $": Line {stackFrame.GetFileLineNumber()}";
                    }

                    message += "\n";
                }

                if (e.TargetSite != null)
                {
                    message += $"\nTarget site {e.TargetSite}";
                }

                if (e.Data.Count >= 1)
                {
                    message += "\n\nExtra information\n";

                    foreach (DictionaryEntry exceptionData in e.Data)
                    {
                        string value;
                        CultureInfo cultureInfo = new CultureInfo("be");
                        cultureInfo.NumberFormat.NumberGroupSeparator = ".";

                        switch(exceptionData.Value)
                        {
                            case null:
                                value = "";
                                break;
                            case int i:
                                value = i.ToString("N0", cultureInfo);
                                break;
                            case long l:
                                value = l.ToString("N0", cultureInfo);
                                break;
                            case string s:
                                value = "\"" + s + "\"";
                                break;
                            default:
                                value = exceptionData.Value.ToString();
                                break;
                        }

                        message += $"\n    {exceptionData.Key} = {value}";
                    }
                }

                if (message.Length > 5000)
                {
                    message += "\n...";
                    break;
                }
            }

            return message;
        }
    }
}
