﻿namespace Tijdsregistratie
{
    static class LoggerControl
    {
        public readonly static NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
    }
}
