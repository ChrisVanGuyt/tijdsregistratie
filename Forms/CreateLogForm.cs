﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Tijdsregistratie.Models;

namespace Tijdsregistratie.Forms
{
    public partial class CreateLogForm : Form
    {
        public DateTime Date
        {
            set => DateDateTimePicker.Value = value.Date;
            get => DateDateTimePicker.Value.Date;
        }

        public TimeOfDayModel TimeOfDay
        {
            set => TimeDateTimePicker.Value = TimeDateTimePicker.MinDate.Date.AddTicks(value.Ticks);
            get => (TimeOfDayModel)TimeDateTimePicker.Value.TimeOfDay;
        }

        public string Description
        {
            set => DescriptionComboBox.Text = value?.Trim() ?? "";
            get => DescriptionComboBox.Text?.Trim() ?? "";
        }

        public IEnumerable<string> Descriptions
        {
            set
            {
                DescriptionComboBox.Items.Clear();
                DescriptionComboBox.Items.AddRange(value.ToArray());
            }
        }

        public CreateLogForm()
        {
            InitializeComponent();
        }
    }
}
