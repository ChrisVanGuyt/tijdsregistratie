﻿namespace Tijdsregistratie.Forms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.DatumDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.TimestampColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.DescriptionColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.DurationColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.LoggingListView = new System.Windows.Forms.ListView();
            this.ListBoxContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.CreateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CopyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.StopToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.DeleteToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.CreateButton = new System.Windows.Forms.Button();
            this.MainMenu = new System.Windows.Forms.MenuStrip();
            this.FileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ExitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bewerkenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.EditCreateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.StopToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stopNuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stopOpTijdstipToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DeleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.StopButtonContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.StopNowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.StopOnTimestampToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SummaryTextBox = new System.Windows.Forms.TextBox();
            this.RefreshSummaryButton = new System.Windows.Forms.Button();
            this.StopButton = new System.Windows.Forms.Button();
            this.ListBoxContextMenuStrip.SuspendLayout();
            this.MainMenu.SuspendLayout();
            this.StopButtonContextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // DatumDateTimePicker
            // 
            this.DatumDateTimePicker.CustomFormat = "ddd d.MMM.yyyy";
            this.DatumDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DatumDateTimePicker.Location = new System.Drawing.Point(50, 25);
            this.DatumDateTimePicker.Margin = new System.Windows.Forms.Padding(2);
            this.DatumDateTimePicker.Name = "DatumDateTimePicker";
            this.DatumDateTimePicker.Size = new System.Drawing.Size(114, 20);
            this.DatumDateTimePicker.TabIndex = 2;
            this.DatumDateTimePicker.ValueChanged += new System.EventHandler(this.DatumDateTimePicker_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 27);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Datum";
            // 
            // TimestampColumnHeader
            // 
            this.TimestampColumnHeader.Text = "Tijdstip";
            this.TimestampColumnHeader.Width = 100;
            // 
            // DescriptionColumnHeader
            // 
            this.DescriptionColumnHeader.Text = "Description";
            this.DescriptionColumnHeader.Width = 100;
            // 
            // DurationColumnHeader
            // 
            this.DurationColumnHeader.Text = "Duur";
            this.DurationColumnHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.DurationColumnHeader.Width = 100;
            // 
            // LoggingListView
            // 
            this.LoggingListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LoggingListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.TimestampColumnHeader,
            this.DescriptionColumnHeader,
            this.DurationColumnHeader});
            this.LoggingListView.ContextMenuStrip = this.ListBoxContextMenuStrip;
            this.LoggingListView.FullRowSelect = true;
            this.LoggingListView.HideSelection = false;
            this.LoggingListView.Location = new System.Drawing.Point(9, 53);
            this.LoggingListView.Margin = new System.Windows.Forms.Padding(2);
            this.LoggingListView.Name = "LoggingListView";
            this.LoggingListView.Size = new System.Drawing.Size(570, 352);
            this.LoggingListView.TabIndex = 5;
            this.LoggingListView.UseCompatibleStateImageBehavior = false;
            this.LoggingListView.View = System.Windows.Forms.View.Details;
            this.LoggingListView.SelectedIndexChanged += new System.EventHandler(this.LoggingListView_SelectedIndexChanged);
            this.LoggingListView.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.LoggingListView_MouseDoubleClick);
            // 
            // ListBoxContextMenuStrip
            // 
            this.ListBoxContextMenuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.ListBoxContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CreateToolStripMenuItem,
            this.CopyToolStripMenuItem,
            this.StopToolStripMenuItem1,
            this.DeleteToolStripMenuItem1});
            this.ListBoxContextMenuStrip.Name = "ListBoxContextMenuStrip";
            this.ListBoxContextMenuStrip.ShowImageMargin = false;
            this.ListBoxContextMenuStrip.Size = new System.Drawing.Size(111, 92);
            this.ListBoxContextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.ListBoxContextMenuStrip_Opening);
            // 
            // CreateToolStripMenuItem
            // 
            this.CreateToolStripMenuItem.Name = "CreateToolStripMenuItem";
            this.CreateToolStripMenuItem.Size = new System.Drawing.Size(110, 22);
            this.CreateToolStripMenuItem.Text = "Nieuw...";
            this.CreateToolStripMenuItem.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.CreateToolStripMenuItem.Click += new System.EventHandler(this.CreateToolStripMenuItem_Click_1);
            // 
            // CopyToolStripMenuItem
            // 
            this.CopyToolStripMenuItem.Name = "CopyToolStripMenuItem";
            this.CopyToolStripMenuItem.Size = new System.Drawing.Size(110, 22);
            this.CopyToolStripMenuItem.Text = "Kopiëren";
            this.CopyToolStripMenuItem.Click += new System.EventHandler(this.CopyToolStripMenuItem_Click);
            // 
            // StopToolStripMenuItem1
            // 
            this.StopToolStripMenuItem1.Name = "StopToolStripMenuItem1";
            this.StopToolStripMenuItem1.Size = new System.Drawing.Size(110, 22);
            this.StopToolStripMenuItem1.Text = "Stop";
            this.StopToolStripMenuItem1.Click += new System.EventHandler(this.StopToolStripMenuItem1_Click);
            // 
            // DeleteToolStripMenuItem1
            // 
            this.DeleteToolStripMenuItem1.Name = "DeleteToolStripMenuItem1";
            this.DeleteToolStripMenuItem1.Size = new System.Drawing.Size(110, 22);
            this.DeleteToolStripMenuItem1.Text = "Verwijderen";
            this.DeleteToolStripMenuItem1.Click += new System.EventHandler(this.DeleteToolStripMenuItem1_Click);
            // 
            // CreateButton
            // 
            this.CreateButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.CreateButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.CreateButton.Location = new System.Drawing.Point(423, 417);
            this.CreateButton.Margin = new System.Windows.Forms.Padding(2);
            this.CreateButton.Name = "CreateButton";
            this.CreateButton.Size = new System.Drawing.Size(75, 23);
            this.CreateButton.TabIndex = 6;
            this.CreateButton.Text = "&Nieuw";
            this.CreateButton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.CreateButton.UseVisualStyleBackColor = true;
            this.CreateButton.Click += new System.EventHandler(this.CreateButton_Click);
            // 
            // MainMenu
            // 
            this.MainMenu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.FileToolStripMenuItem,
            this.bewerkenToolStripMenuItem});
            this.MainMenu.Location = new System.Drawing.Point(0, 0);
            this.MainMenu.Name = "MainMenu";
            this.MainMenu.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.MainMenu.Size = new System.Drawing.Size(586, 24);
            this.MainMenu.TabIndex = 0;
            this.MainMenu.Text = "Hoofdmenu";
            // 
            // FileToolStripMenuItem
            // 
            this.FileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ExitToolStripMenuItem});
            this.FileToolStripMenuItem.Name = "FileToolStripMenuItem";
            this.FileToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.FileToolStripMenuItem.Text = "&Bestand";
            // 
            // ExitToolStripMenuItem
            // 
            this.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem";
            this.ExitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.ExitToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.ExitToolStripMenuItem.Text = "Afsluiten";
            // 
            // bewerkenToolStripMenuItem
            // 
            this.bewerkenToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.EditCreateToolStripMenuItem,
            this.StopToolStripMenuItem,
            this.DeleteToolStripMenuItem});
            this.bewerkenToolStripMenuItem.Name = "bewerkenToolStripMenuItem";
            this.bewerkenToolStripMenuItem.Size = new System.Drawing.Size(70, 20);
            this.bewerkenToolStripMenuItem.Text = "Be&werken";
            // 
            // EditCreateToolStripMenuItem
            // 
            this.EditCreateToolStripMenuItem.Name = "EditCreateToolStripMenuItem";
            this.EditCreateToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.EditCreateToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.EditCreateToolStripMenuItem.Text = "&Nieuw";
            this.EditCreateToolStripMenuItem.Click += new System.EventHandler(this.CreateToolStripMenuItem_Click);
            // 
            // StopToolStripMenuItem
            // 
            this.StopToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stopNuToolStripMenuItem,
            this.stopOpTijdstipToolStripMenuItem});
            this.StopToolStripMenuItem.Name = "StopToolStripMenuItem";
            this.StopToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.StopToolStripMenuItem.Text = "&Stop";
            // 
            // stopNuToolStripMenuItem
            // 
            this.stopNuToolStripMenuItem.Name = "stopNuToolStripMenuItem";
            this.stopNuToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.stopNuToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.stopNuToolStripMenuItem.Text = "Stop nu";
            this.stopNuToolStripMenuItem.Click += new System.EventHandler(this.StopNowToolStripMenuItem_Click);
            // 
            // stopOpTijdstipToolStripMenuItem
            // 
            this.stopOpTijdstipToolStripMenuItem.Name = "stopOpTijdstipToolStripMenuItem";
            this.stopOpTijdstipToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.stopOpTijdstipToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.stopOpTijdstipToolStripMenuItem.Text = "Stop op tijdstip";
            this.stopOpTijdstipToolStripMenuItem.Click += new System.EventHandler(this.StopOpTijdstipToolStripMenuItem_Click);
            // 
            // DeleteToolStripMenuItem
            // 
            this.DeleteToolStripMenuItem.Name = "DeleteToolStripMenuItem";
            this.DeleteToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.DeleteToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
            this.DeleteToolStripMenuItem.Text = "Verwijderen";
            this.DeleteToolStripMenuItem.Click += new System.EventHandler(this.DeleteToolStripMenuItem_Click);
            // 
            // StopButtonContextMenuStrip
            // 
            this.StopButtonContextMenuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.StopButtonContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StopNowToolStripMenuItem,
            this.StopOnTimestampToolStripMenuItem});
            this.StopButtonContextMenuStrip.Name = "CreateButtonContextMenuStrip";
            this.StopButtonContextMenuStrip.Size = new System.Drawing.Size(155, 48);
            this.StopButtonContextMenuStrip.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.StopButtonContextMenuStrip_ItemClicked);
            // 
            // StopNowToolStripMenuItem
            // 
            this.StopNowToolStripMenuItem.Name = "StopNowToolStripMenuItem";
            this.StopNowToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.StopNowToolStripMenuItem.Text = "Stop nu";
            this.StopNowToolStripMenuItem.Click += new System.EventHandler(this.StopNowToolStripMenuItem_Click);
            // 
            // StopOnTimestampToolStripMenuItem
            // 
            this.StopOnTimestampToolStripMenuItem.Name = "StopOnTimestampToolStripMenuItem";
            this.StopOnTimestampToolStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.StopOnTimestampToolStripMenuItem.Text = "Stop op tijdstip";
            // 
            // SummaryTextBox
            // 
            this.SummaryTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SummaryTextBox.BackColor = System.Drawing.SystemColors.Control;
            this.SummaryTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SummaryTextBox.Location = new System.Drawing.Point(168, 28);
            this.SummaryTextBox.Margin = new System.Windows.Forms.Padding(2);
            this.SummaryTextBox.Name = "SummaryTextBox";
            this.SummaryTextBox.ReadOnly = true;
            this.SummaryTextBox.Size = new System.Drawing.Size(383, 13);
            this.SummaryTextBox.TabIndex = 3;
            this.SummaryTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // RefreshSummaryButton
            // 
            this.RefreshSummaryButton.Image = global::Tijdsregistratie.Properties.Resources.Refresh_16x;
            this.RefreshSummaryButton.Location = new System.Drawing.Point(556, 25);
            this.RefreshSummaryButton.Name = "RefreshSummaryButton";
            this.RefreshSummaryButton.Size = new System.Drawing.Size(23, 23);
            this.RefreshSummaryButton.TabIndex = 4;
            this.RefreshSummaryButton.UseVisualStyleBackColor = true;
            this.RefreshSummaryButton.Click += new System.EventHandler(this.RefreshSummaryButton_Click);
            // 
            // StopButton
            // 
            this.StopButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.StopButton.Image = global::Tijdsregistratie.Properties.Resources.GlyphDown_16x_1_;
            this.StopButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.StopButton.Location = new System.Drawing.Point(502, 417);
            this.StopButton.Margin = new System.Windows.Forms.Padding(2);
            this.StopButton.Name = "StopButton";
            this.StopButton.Size = new System.Drawing.Size(75, 23);
            this.StopButton.TabIndex = 7;
            this.StopButton.Text = "&Stop";
            this.StopButton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.StopButton.UseVisualStyleBackColor = true;
            this.StopButton.Click += new System.EventHandler(this.StopButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(586, 449);
            this.Controls.Add(this.RefreshSummaryButton);
            this.Controls.Add(this.SummaryTextBox);
            this.Controls.Add(this.StopButton);
            this.Controls.Add(this.CreateButton);
            this.Controls.Add(this.LoggingListView);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DatumDateTimePicker);
            this.Controls.Add(this.MainMenu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.MainMenu;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "MainForm";
            this.Text = "Tijdregistratie";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ListBoxContextMenuStrip.ResumeLayout(false);
            this.MainMenu.ResumeLayout(false);
            this.MainMenu.PerformLayout();
            this.StopButtonContextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker DatumDateTimePicker;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ColumnHeader TimestampColumnHeader;
        private System.Windows.Forms.ColumnHeader DescriptionColumnHeader;
        private System.Windows.Forms.ColumnHeader DurationColumnHeader;
        private System.Windows.Forms.ListView LoggingListView;
        private System.Windows.Forms.Button CreateButton;
        private System.Windows.Forms.MenuStrip MainMenu;
        private System.Windows.Forms.ToolStripMenuItem bewerkenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem EditCreateToolStripMenuItem;
        private System.Windows.Forms.Button StopButton;
        private System.Windows.Forms.ToolStripMenuItem StopToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip ListBoxContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem CreateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem StopToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem FileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ExitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem DeleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem DeleteToolStripMenuItem1;
        private System.Windows.Forms.ContextMenuStrip StopButtonContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem StopNowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem StopOnTimestampToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stopNuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stopOpTijdstipToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CopyToolStripMenuItem;
        private System.Windows.Forms.TextBox SummaryTextBox;
        private System.Windows.Forms.Button RefreshSummaryButton;
    }
}

