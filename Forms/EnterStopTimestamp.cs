﻿using System;
using System.Windows.Forms;
using Tijdsregistratie.Models;

namespace Tijdsregistratie.Forms
{
    public partial class EnterStopTimestamp : Form
    {
        public TimeOfDayModel TimeOfDay
        {
            get => (TimeOfDayModel)TimestampDateTimePicker.Value.TimeOfDay;
            set => TimestampDateTimePicker.Value = TimestampDateTimePicker.MinDate.Date + (TimeSpan)(value ?? TimeOfDayModel.Zero);
        }

        public TimeOfDayModel MinTime
        {
            get => (TimeOfDayModel)TimestampDateTimePicker.MinDate.TimeOfDay;
            set => TimestampDateTimePicker.MinDate = TimestampDateTimePicker.MinDate.Date + (TimeSpan)(value ?? TimeOfDayModel.Zero);
        }

        public TimeOfDayModel MaxTime
        {
            get => (TimeOfDayModel)TimestampDateTimePicker.MaxDate.TimeOfDay;
            set => TimestampDateTimePicker.MaxDate = TimestampDateTimePicker.MinDate.Date + (TimeSpan)(value ?? TimeOfDayModel.Zero);
        }

        public EnterStopTimestamp()
        {
            try
            {
                InitializeComponent();
            }
            catch (Exception ex) { ExceptionDialog.Show(ex.Message, "Stoptijd", ex); }
        }
    }
}
