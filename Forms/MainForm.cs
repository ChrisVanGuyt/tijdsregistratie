﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Tijdsregistratie.Controls;
using Tijdsregistratie.Models;

namespace Tijdsregistratie.Forms
{
    public partial class MainForm : Form
    {
        // Initialiseer _timeRegistration voor het geval de read van het bestand bij openen zou mislukken.
        TimeRegistrationModel _timeRegistration = new TimeRegistrationModel();

        public MainForm()
        {
            try
            {
                InitializeComponent();
            }
            catch (Exception ex) { HandleException(ex); }
        }

        private static void HandleException(Exception value)
        {
            try
            {
                LoggerControl.Logger.Error(value);
            }
            catch (Exception ex)
            {
                ExceptionDialog.Show(value.Message, "Hoofdscherm", value);
                ExceptionDialog.Show(value.Message, "Handle exception", ex);
            }

            ExceptionDialog.Show(value.Message, "Hoofdscherm", value);
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                _timeRegistration.Stop();
            }
            catch (Exception ex) { HandleException(ex); }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            try
            {
                _timeRegistration = DatabaseControl.Read(@"U:\Chris\Urenregistratie\database.xml");
                _timeRegistration.StartLoggingIfNotRunning();
                DatumDateTimePicker.Value = DateTime.Today;
            }
            catch (Exception ex) { HandleException(ex); }
        }

        private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (LoggingListView.SelectedItems.Count == 0)
                {
                    return;
                }

                ListViewItem selectedItem = LoggingListView.SelectedItems[0];

                if (selectedItem.Tag is null)
                {
                    _timeRegistration.Stop(DateTime.Now.Date, TimeOfDayModel.Now);
                }
                else
                {
                    LogModel from = (LogModel)selectedItem.Tag;
                    _timeRegistration.CreateLog(DateTime.Now.Date, TimeOfDayModel.Now, ((LogModel)selectedItem.Tag).Description);
                }

                WriteToDatabase();
                UpdateScreen();
            }
            catch (Exception ex) { HandleException(ex); }
        }

        private void CreateButton_Click(object sender, EventArgs e)
        {
            try
            {
                CreateRecord();
            }
            catch (Exception ex) { HandleException(ex); }
        }

        private void CreateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                CreateRecord();
            }
            catch (Exception ex) { HandleException(ex); }
        }

        private void CreateToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            try
            {
                CreateRecord();
            }
            catch (Exception ex) { HandleException(ex); }
        }

        private void DatumDateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                UpdateScreen();
            }
            catch (Exception ex) { HandleException(ex); }
        }

        private void DeleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Delete();
            }
            catch (Exception ex) { HandleException(ex); }
        }

        private void DeleteToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            try
            {
                Delete();
            }
            catch (Exception ex) { HandleException(ex); }
        }

        private void ListBoxContextMenuStrip_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                StopToolStripMenuItem1.Enabled = StopMakesSense() || LoggingListView.SelectedItems.Count >= 1;
                DeleteToolStripMenuItem1.Enabled = LoggingListView.SelectedItems.Count >= 1;
            }
            catch (Exception ex) { HandleException(ex); }
        }

        private void LoggingListView_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            try
            {
                if (sender is ListView listview)
                {
                    ListViewHitTestInfo info = listview.HitTest(e.X, e.Y);

                    if (info.Item?.Tag is LogModel log)
                    {
                        UpdateRow(log);
                    }
                    else if (LoggingListView.Items[info.Item.Index - 1].Tag is LogModel previous)
                    {
                        LogModel next = _timeRegistration.GetNextLog(DatumDateTimePicker.Value, previous);

                        EnterStopTimestamp dlg = new EnterStopTimestamp
                        {
                            MinTime = previous.BeginTime,
                            MaxTime = _timeRegistration.GetNextLog(DatumDateTimePicker.Value, previous)?.BeginTime ?? TimeOfDayModel.FromHours(24),
                            TimeOfDay = previous.EndTime
                        };

                        DialogResult result = dlg.ShowDialog();

                        switch (result)
                        {
                            case DialogResult.OK:
                                Stop(dlg.TimeOfDay);
                                break;
                            case DialogResult.Cancel:
                                break;
                            default:
                                throw new NotImplementedException($"Dialog result {result} not supported.");
                        }
                    }
                }
            }
            catch (Exception ex) { HandleException(ex); }
        }

        private void LoggingListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                EnableDeleteMenuItem();
            }
            catch (Exception ex) { HandleException(ex); }
        }

        private void RefreshSummaryButton_Click(object sender, EventArgs e)
        {
            try
            {
                UpdateSummaryTextBox();
            }
            catch (Exception ex) { HandleException(ex); }
        }

        private void StopButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (sender is Button button)
                {
                    if (!StopMakesSense())
                    {
                        StopOnTimestamp();
                    }
                    else if (ClickIsOnDownArrow(button, e as MouseEventArgs))
                    {
                        ShowMenuUnderStopButton();
                    }
                    else
                    {
                        Stop(TimeOfDayModel.Now);
                    }
                }
            }
            catch (Exception ex) { HandleException(ex); }
        }

        private void StopButtonContextMenuStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                if (e.ClickedItem == StopNowToolStripMenuItem)
                {
                    Stop(TimeOfDayModel.Now);
                }
                else if (e.ClickedItem == StopOnTimestampToolStripMenuItem)
                {
                    StopOnTimestamp();
                }
            }
            catch (Exception ex) { HandleException(ex); }
        }

        private void StopNowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Stop(TimeOfDayModel.Now);
            }
            catch (Exception ex) { HandleException(ex); }
        }

        private void StopOpTijdstipToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                StopOnTimestamp();
            }
            catch (Exception ex) { HandleException(ex); }
        }

        private void StopToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            try
            {
                if (LoggingListView.SelectedItems.Count >= 1)
                {
                    TimeOfDayModel minTime = null;
                    TimeOfDayModel maxTime = null;

                    foreach (ListViewItem selectedItem in LoggingListView.SelectedItems)
                    {
                        if (selectedItem?.Tag is LogModel log)
                        {
                            if (minTime is null || minTime > log.BeginTime)
                            {
                                minTime = log.BeginTime;
                            }

                            if (maxTime is null || maxTime < log.EndTime)
                            {
                                maxTime = log.EndTime;
                            }
                        }
                    }

                    EnterStopTimestamp dlg = new EnterStopTimestamp
                    {
                        MinTime = minTime,
                        MaxTime = maxTime
                    };

                    DialogResult result = dlg.ShowDialog();

                    switch (result)
                    {
                        case DialogResult.OK:
                            Stop(dlg.TimeOfDay);
                            break;
                        case DialogResult.Cancel:
                            break;
                        default:
                            throw new NotImplementedException($"Dialog result {result} not supported.");
                    }
                }
                else
                {
                    Stop(TimeOfDayModel.Now);
                }
            }
            catch (Exception ex) { HandleException(ex); }
        }

        private bool ClickIsOnDownArrow(Button button, MouseEventArgs e)
        {
            return e.X >= button.Width - button.Image.Width;
        }

        private void CreateRecord()
        {
            CreateLogForm dlg = new CreateLogForm
            {
                Date = DatumDateTimePicker.Value,
                Descriptions = _timeRegistration.RecentDescriptions
            };

            DialogResult result = dlg.ShowDialog();

            switch (result)
            {
                case DialogResult.Cancel:
                    return;
                case DialogResult.OK:
                    _timeRegistration.CreateLog(dlg.Date, dlg.TimeOfDay, dlg.Description);
                    WriteToDatabase();
                    UpdateScreen();
                    return;
                default:
                    throw new NotImplementedException($"DialogResult {result} not implemented.");
            }
        }

        private void Delete()
        {
            if (LoggingListView.SelectedItems.Count > 3)
            {
                DialogResult choice = MessageBox.Show($"Verwijder {LoggingListView.SelectedItems.Count} rijen?", "Verwijderen", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);

                switch (choice)
                {
                    case DialogResult.Yes:
                        foreach (ListViewItem item in LoggingListView.SelectedItems)
                        {
                            if (item.Tag is LogModel log)
                            {
                                _timeRegistration.Delete(DatumDateTimePicker.Value, log);
                            }
                            else if (LoggingListView.Items[item.Index - 1].Tag is LogModel previous)
                            {
                                _timeRegistration.DeleteStop(DatumDateTimePicker.Value, previous);
                            }
                        }

                        break;
                    case DialogResult.No:
                        break;
                    default:
                        throw new NotImplementedException($"DialogResult {choice} not implemented.");
                }
            }
            else
            {
                foreach (ListViewItem item in LoggingListView.SelectedItems)
                {
                    if (item.Tag is LogModel log)
                    {
                        DialogResult choice = MessageBox.Show($"Verwijder rij {log.BeginTime}, \"{log.Description}\"?", "Verwijderen", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);

                        switch (choice)
                        {
                            case DialogResult.Yes:
                                _timeRegistration.Delete(DatumDateTimePicker.Value, log);
                                break;
                            case DialogResult.No:
                                break;
                            default:
                                throw new NotImplementedException($"DialogResult {choice} not implemented.");
                        }
                    }
                    else if (LoggingListView.Items[item.Index - 1].Tag is LogModel previous)
                    {
                        DialogResult choice = MessageBox.Show($"Verwijder stop om {previous.EndTime}?", "Verwijderen", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);

                        switch (choice)
                        {
                            case DialogResult.Yes:
                                _timeRegistration.DeleteStop(DatumDateTimePicker.Value, previous);
                                break;
                            case DialogResult.No:
                                break;
                            default:
                                throw new NotImplementedException($"DialogResult {choice} not implemented.");
                        }
                    }
                }
            }

            WriteToDatabase();
            UpdateScreen();
        }

        private void DisplayRow(TimeOfDayModel timeOfDay, string description, TimeSpan? duration, object tag)
        {
            ListViewItem lvi = new ListViewItem(new[] {
                timeOfDay.ToString(),
                description ?? "",
                duration?.ToString(@"h\:mm") ?? ""
            })
            {
                Tag = tag
            };

            lvi.UseItemStyleForSubItems = false;
            lvi.SubItems[0].ForeColor = lvi.SubItems[1].ForeColor = tag == null || duration == null ? Color.Red : Color.Black;
            lvi.SubItems[2].ForeColor = Color.Blue;
            LoggingListView.Items.Add(lvi);
        }

        private void EnableDeleteMenuItem()
        {
            CopyToolStripMenuItem.Enabled = DeleteToolStripMenuItem.Enabled = LoggingListView.SelectedItems.Count >= 1;
        }

        private void UpdateScreen()
        {
            UpdateSummaryTextBox();

            try
            {
                LoggingListView.BeginUpdate();
                LoggingListView.Items.Clear();
                TimeOfDayModel runningTimestamp = null;
                IEnumerable<LogModel> rows = _timeRegistration.GetDayLogs(DatumDateTimePicker.Value);

                foreach (LogModel log in rows.OrderBy(row => row.BeginTime))
                {
                    if (runningTimestamp != null && log.BeginTime > runningTimestamp)
                    {
                        DisplayRow(runningTimestamp, null, null, null);
                    }

                    DisplayRow(log.BeginTime, log.Description, log.Duration, log);
                    runningTimestamp = log.BeginTime + (log.Duration ?? TimeSpan.Zero);
                }

                if (rows.Any())
                {
                    LogModel lastRow = rows.OrderBy(row => row.BeginTime).Last();

                    if (lastRow.EndTime != null)
                    {
                        DisplayRow(lastRow.EndTime, null, null, null);
                    }
                }

                LoggingListView.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);

                if (rows.Any())
                {
                    int[] columnWidths = new int[LoggingListView.Columns.Count];

                    for (int i = 0; i < LoggingListView.Columns.Count; i++)
                    {
                        columnWidths[i] = LoggingListView.Columns[i].Width;
                    }

                    LoggingListView.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);

                    for (int i = 0; i < LoggingListView.Columns.Count; i++)
                    {
                        if (LoggingListView.Columns[i].Width < columnWidths[i])
                        {
                            LoggingListView.Columns[i].Width = columnWidths[i];
                        }
                    }
                }
            }
            finally
            {
                LoggingListView.EndUpdate();
            }

            StopButton.Enabled = LoggingListView.Items.Count != 0;
            StopToolStripMenuItem.Enabled = StopMakesSense();
            EnableDeleteMenuItem();
        }

        public void ShowMenuUnderStopButton()
        {
            StopButtonContextMenuStrip.Show(StopButton, new Point(StopButton.Width - StopButton.Image.Width, StopButton.Height), ToolStripDropDownDirection.Default);
        }

        private void Stop(TimeOfDayModel timeOfDay)
        {
            _timeRegistration.Stop(DatumDateTimePicker.Value, timeOfDay);
            WriteToDatabase();
            UpdateScreen();
        }

        private bool StopMakesSense() => !_timeRegistration.LastLogIsStopped(DatumDateTimePicker.Value);

        private void StopOnTimestamp()
        {
            EnterStopTimestamp dlg = new EnterStopTimestamp
            {
                TimeOfDay = TimeOfDayModel.Now
            };

            DialogResult result = dlg.ShowDialog();

            switch (result)
            {
                case DialogResult.OK:
                    Stop(dlg.TimeOfDay);
                    break;
                case DialogResult.Cancel:
                    break;
                default:
                    throw new NotImplementedException($"Dialog result {result} not supported.");
            }
        }

        private void UpdateRow(LogModel log)
        {
            CreateLogForm dlg = new CreateLogForm
            {
                Date = DatumDateTimePicker.Value,
                Description = log.Description,
                Descriptions = _timeRegistration.RecentDescriptions,
                TimeOfDay = log.BeginTime
            };

            DialogResult result = dlg.ShowDialog();

            switch (result)
            {
                case DialogResult.Cancel:
                    return;
                case DialogResult.OK:
                    if (dlg.Date == DatumDateTimePicker.Value)
                    {
                        _timeRegistration.UpdateLog(log, dlg.Date, dlg.TimeOfDay, dlg.Description);
                    }
                    else
                    {
                        _timeRegistration.Delete(DatumDateTimePicker.Value, log);
                        _timeRegistration.CreateLog(dlg.Date, dlg.TimeOfDay, dlg.Description);
                    }

                    WriteToDatabase();
                    UpdateScreen();
                    return;
                default:
                    throw new NotImplementedException($"DialogResult {result} not implemented.");
            }

        }

        private void UpdateSummaryTextBox()
        {
            if(_timeRegistration is null)
            {
                SummaryTextBox.Text = "";
                return;
            }

            TimeSpan weekTotal = _timeRegistration.GetWeekTotal(DateTime.Now.Date);
            TimeSpan weekPauze = _timeRegistration.GetWeekPauze(DateTime.Now.Date);
            TimeSpan minimumPauze = TimeSpan.FromHours(0.5d * (double)DateTime.Now.Date.DayOfWeek);
            TimeSpan extraPauze = minimumPauze > weekPauze ? minimumPauze - weekPauze : TimeSpan.Zero;
            TimeOfDayModel dayEndBasedOnWeek = TimeOfDayModel.Now + TimeSpan.FromHours(8d * (double)DateTime.Now.Date.DayOfWeek) - weekTotal + extraPauze;
            TimeSpan dayTotal = _timeRegistration.GetDayTotal(DatumDateTimePicker.Value.Date);
            SummaryTextBox.Text = $"{(TimeOfDayModel)weekTotal} / {dayTotal:h\\:mm} / {dayEndBasedOnWeek}";
        }

        private void WriteToDatabase()
        {
            DatabaseControl.Write(_timeRegistration, @"U:\Chris\Urenregistratie\database.xml");
        }
    }
}
