﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;
using Tijdsregistratie.Controls;

namespace Tijdsregistratie.Forms
{
    public partial class ExceptionDialog : Form
    {
        private Exception _exeption = null;

        private readonly Image _initialDetailsButtonImage = null;

        private string _message = null;

        private bool _showDetails = false;

        private DateTime? _timeStamp = null;

        private readonly IPAddress _interNetworkIpAddress = GetIpAddress(AddressFamily.InterNetwork);
        private readonly IPAddress _interNetworkV6IpAddress = GetIpAddress(AddressFamily.InterNetworkV6);
        private readonly string _assemblyVersion;
        private int _windowHeightAtStart;
        private int _buttonRowFromBottomAtStart;
        private int _messageTextBoxBottomFromButtons;
        private Size _minimumSizeAtStart;

        public bool CopyButtonEnabled
        {
            get => CopyButton.Enabled;
            set => CopyButton.Enabled = value;
        }

        public Exception Exception
        {
            get => _exeption;
            set
            {
                _timeStamp = DateTime.Now;
                _exeption = value;
                DetailsTextBox.Text = DetailsMessage.Replace("\n", "\r\n");
            }
        }

        public string Message
        {
            get => _message;
            set
            {
                MessageTextBox.Text = _message = value;
                MessageTextBox.SelectionStart = MessageTextBox.Text.Length;
                MessageTextBox.DeselectAll();
            }
        }

        private string DetailsMessage
        {
            get
            {
                return string.Join(Environment.NewLine, new[]
                {
                    $"DateAndTime: {_timeStamp:G}",
                    string.Empty,
                    $"CommandLine: {Environment.CommandLine}",
                    $"MachineName: {Environment.MachineName}",
                    $"OsVersion: {Environment.OSVersion.VersionString}",
                    $"User: {Environment.UserDomainName}\\{Environment.UserName}",
                    $"Version of the common language runtime: {Environment.Version}",
                    string.Empty,
                    $"IPv4Address: {_interNetworkIpAddress}",
                    $"IPv6Address: {_interNetworkV6IpAddress}",
                    $"ApplicationDomain: {AppDomain.CurrentDomain.FriendlyName}",
                    $"AssembyName: {GetType().Assembly.ManifestModule.Name}",
                    $"AssembyVersion: {_assemblyVersion}",
                    string.Empty,
                    Logging.ExceptionToMessage(_exeption)
                });
            }
        }

        /// <summary>
        /// Getting the IP address of the local machine
        /// </summary>
        /// <returns></returns>
        private static IPAddress GetIpAddress(AddressFamily addressFamily)
        {
            try
            {
                string strHostName = Dns.GetHostName();
                IPAddress[] ipAddresses = Dns.GetHostEntry(strHostName).AddressList;

                if (ipAddresses == null || ipAddresses.Length == 0)
                {
                    return null;
                }

                foreach (IPAddress ipAddress in ipAddresses)
                {
                    if (ipAddress.AddressFamily == addressFamily)
                    {
                        return ipAddress;
                    }
                }
                return ipAddresses[0];
            }
            catch
            {
                return null;
            }
        }

        public ExceptionDialog()
        {
            InitializeComponent();
        }

        public ExceptionDialog(string message, string caption, Exception exception)
        {
            InitializeComponent();
            Text = caption;
            _initialDetailsButtonImage = DetailsButton.Image;
            _assemblyVersion = FileVersionInfo.GetVersionInfo(GetType().Assembly.Location).FileVersion;
            DetailsTextBox.Visible = false;
            Message = message;
            Exception = exception;
            RememberStartLocations();
        }

        private void RememberStartLocations()
        {
            _windowHeightAtStart = Height;
            _buttonRowFromBottomAtStart = Height - OkButton.Bottom;
            _messageTextBoxBottomFromButtons = OkButton.Top - MessageTextBox.Height;
            _minimumSizeAtStart = MinimumSize;
        }

        private void OkButton_Click(object sender, EventArgs e) => DialogResult = DialogResult.OK;

        private void DetailsButton_Click(object sender, EventArgs e)
        {
            Button[] buttonRow = new[] { DetailsButton, CopyButton, OkButton };

            _showDetails = !_showDetails;

            if (_showDetails)
            {
                if (Height > 400)
                {
                    // hoogte wordt te groot, dus herstellen naar originele instellingen
                    MinimumSize = _minimumSizeAtStart;
                    Height = _windowHeightAtStart;
                }

                foreach (Button button in buttonRow)
                {
                    button.Top = Height - button.Height - _buttonRowFromBottomAtStart;
                    button.Anchor = (button.Anchor & ~AnchorStyles.Bottom) | AnchorStyles.Top;
                }

                MessageTextBox.Anchor &= ~AnchorStyles.Bottom;
                MessageTextBox.Height = OkButton.Top - _messageTextBoxBottomFromButtons;
                Height = 768;
                MinimumSize = new Size(_minimumSizeAtStart.Width, OkButton.Bottom + _buttonRowFromBottomAtStart + 50);
                DetailsTextBox.Top = OkButton.Bottom + 10;
                DetailsTextBox.Height = Height - DetailsTextBox.Top + PointToClient(Location).Y;
                DetailsTextBox.Height = ClientSize.Height - DetailsTextBox.Top;
                DetailsTextBox.Anchor |= AnchorStyles.Top | AnchorStyles.Bottom;
                DetailsTextBox.Enabled = true;
                DetailsTextBox.Visible = true;
            }
            else
            {
                DetailsTextBox.Visible = false;
                DetailsTextBox.Enabled = false;
                MinimumSize = _minimumSizeAtStart;
                Height = OkButton.Bottom + _buttonRowFromBottomAtStart;
                MessageTextBox.Anchor |= AnchorStyles.Bottom;

                foreach (Button button in buttonRow)
                {
                    button.Anchor = (button.Anchor & ~AnchorStyles.Top) | AnchorStyles.Bottom;
                }

                DetailsButton.Image = _initialDetailsButtonImage;
                MinimumSize = _minimumSizeAtStart;
            }
        }

        public static void Show(string message, string caption, Exception ex) => new ExceptionDialog(message, caption, ex).ShowDialog();

        public static void Show(string message, string caption, Exception ex, bool enableCopyButton)
        {
            new ExceptionDialog(message, caption, ex)
            {
                CopyButtonEnabled = enableCopyButton
            }.ShowDialog();
        }

        private void CopyButton_Click(object sender, EventArgs e) => Clipboard.SetText(DetailsMessage);
    }
}
