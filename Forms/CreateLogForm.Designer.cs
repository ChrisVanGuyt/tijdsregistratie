﻿namespace Tijdsregistratie.Forms
{
    partial class CreateLogForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreateLogForm));
            this.DateDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.TimeDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.OKButton = new System.Windows.Forms.Button();
            this.ExitButton = new System.Windows.Forms.Button();
            this.DescriptionComboBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // DateDateTimePicker
            // 
            this.DateDateTimePicker.CustomFormat = "ddd d.MMM.yyyy";
            this.DateDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.DateDateTimePicker.Location = new System.Drawing.Point(12, 12);
            this.DateDateTimePicker.Name = "DateDateTimePicker";
            this.DateDateTimePicker.Size = new System.Drawing.Size(145, 22);
            this.DateDateTimePicker.TabIndex = 0;
            // 
            // TimeDateTimePicker
            // 
            this.TimeDateTimePicker.CustomFormat = "H:mm";
            this.TimeDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.TimeDateTimePicker.Location = new System.Drawing.Point(163, 12);
            this.TimeDateTimePicker.Name = "TimeDateTimePicker";
            this.TimeDateTimePicker.ShowUpDown = true;
            this.TimeDateTimePicker.Size = new System.Drawing.Size(80, 22);
            this.TimeDateTimePicker.TabIndex = 1;
            // 
            // OKButton
            // 
            this.OKButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.OKButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.OKButton.Location = new System.Drawing.Point(594, 81);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(85, 25);
            this.OKButton.TabIndex = 3;
            this.OKButton.Text = "OK";
            this.OKButton.UseVisualStyleBackColor = true;
            // 
            // ExitButton
            // 
            this.ExitButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ExitButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.ExitButton.Location = new System.Drawing.Point(685, 81);
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new System.Drawing.Size(85, 25);
            this.ExitButton.TabIndex = 4;
            this.ExitButton.Text = "Annuleren";
            this.ExitButton.UseVisualStyleBackColor = true;
            // 
            // DescriptionComboBox
            // 
            this.DescriptionComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DescriptionComboBox.Location = new System.Drawing.Point(13, 41);
            this.DescriptionComboBox.Name = "DescriptionComboBox";
            this.DescriptionComboBox.Size = new System.Drawing.Size(757, 24);
            this.DescriptionComboBox.TabIndex = 2;
            // 
            // CreateLogForm
            // 
            this.AcceptButton = this.OKButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.ExitButton;
            this.ClientSize = new System.Drawing.Size(782, 118);
            this.ControlBox = false;
            this.Controls.Add(this.DescriptionComboBox);
            this.Controls.Add(this.ExitButton);
            this.Controls.Add(this.OKButton);
            this.Controls.Add(this.TimeDateTimePicker);
            this.Controls.Add(this.DateDateTimePicker);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1024, 165);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(270, 165);
            this.Name = "CreateLogForm";
            this.Text = "Toevoegen";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DateTimePicker DateDateTimePicker;
        private System.Windows.Forms.DateTimePicker TimeDateTimePicker;
        private System.Windows.Forms.Button OKButton;
        private System.Windows.Forms.Button ExitButton;
        private System.Windows.Forms.ComboBox DescriptionComboBox;
    }
}