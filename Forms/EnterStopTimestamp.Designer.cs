﻿namespace Tijdsregistratie.Forms
{
    partial class EnterStopTimestamp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EnterStopTimestamp));
            this.TimestampLabel = new System.Windows.Forms.Label();
            this.TimestampDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.ExitButton = new System.Windows.Forms.Button();
            this.OKButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // TimestampLabel
            // 
            this.TimestampLabel.AutoSize = true;
            this.TimestampLabel.Location = new System.Drawing.Point(10, 12);
            this.TimestampLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.TimestampLabel.Name = "TimestampLabel";
            this.TimestampLabel.Size = new System.Drawing.Size(40, 13);
            this.TimestampLabel.TabIndex = 0;
            this.TimestampLabel.Text = "Tijdstip";
            // 
            // TimestampDateTimePicker
            // 
            this.TimestampDateTimePicker.CustomFormat = "HH:mm";
            this.TimestampDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.TimestampDateTimePicker.Location = new System.Drawing.Point(82, 10);
            this.TimestampDateTimePicker.Margin = new System.Windows.Forms.Padding(2);
            this.TimestampDateTimePicker.MaxDate = new System.DateTime(9998, 1, 31, 0, 0, 0, 0);
            this.TimestampDateTimePicker.Name = "TimestampDateTimePicker";
            this.TimestampDateTimePicker.ShowUpDown = true;
            this.TimestampDateTimePicker.Size = new System.Drawing.Size(60, 20);
            this.TimestampDateTimePicker.TabIndex = 1;
            this.TimestampDateTimePicker.Value = new System.DateTime(2001, 1, 1, 0, 0, 0, 0);
            // 
            // ExitButton
            // 
            this.ExitButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.ExitButton.Location = new System.Drawing.Point(77, 41);
            this.ExitButton.Margin = new System.Windows.Forms.Padding(2);
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new System.Drawing.Size(64, 20);
            this.ExitButton.TabIndex = 3;
            this.ExitButton.Text = "Annuleren";
            this.ExitButton.UseVisualStyleBackColor = true;
            // 
            // OKButton
            // 
            this.OKButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.OKButton.Location = new System.Drawing.Point(9, 41);
            this.OKButton.Margin = new System.Windows.Forms.Padding(2);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(64, 20);
            this.OKButton.TabIndex = 2;
            this.OKButton.Text = "OK";
            this.OKButton.UseVisualStyleBackColor = true;
            // 
            // EnterStopTimestamp
            // 
            this.AcceptButton = this.OKButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.ExitButton;
            this.ClientSize = new System.Drawing.Size(150, 72);
            this.ControlBox = false;
            this.Controls.Add(this.ExitButton);
            this.Controls.Add(this.OKButton);
            this.Controls.Add(this.TimestampDateTimePicker);
            this.Controls.Add(this.TimestampLabel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EnterStopTimestamp";
            this.Text = "Voer stoptijd in";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label TimestampLabel;
        private System.Windows.Forms.DateTimePicker TimestampDateTimePicker;
        private System.Windows.Forms.Button ExitButton;
        private System.Windows.Forms.Button OKButton;
    }
}