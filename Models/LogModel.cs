﻿using System;
using System.Reflection;
using Tijdsregistratie.Models.DabaseModel;

namespace Tijdsregistratie.Models
{
    public class LogModel
    {
        public string Description { get; set; }

        public TimeOfDayModel BeginTime { get; set; }

        public TimeOfDayModel EndTime { get; set; }

        public static explicit operator LogModel(timeregistrationDayLogging value)
        {
            return new LogModel
            {
                Description = value.description,
                BeginTime = (TimeOfDayModel)value.begin.TimeOfDay,
                EndTime = value.endSpecified ? (TimeOfDayModel)value.end.TimeOfDay : null
            };
        }

        public TimeSpan? Duration
        {
            get
            {
                LoggerControl.Logger.Trace($"get {GetType().FullName}.Duration");
                TimeSpan? value = EndTime is null ? (TimeSpan?)null : (TimeSpan)(EndTime - BeginTime);
                LoggerControl.Logger.Trace($"get {GetType().FullName}.Duration => {value}");
                return value;
            }

            set
            {
                LoggerControl.Logger.Trace($"set {GetType().FullName}.Duration(nameof(value)={value})");
                EndTime = value is null ? null : BeginTime + value.Value;
            }
        }
    }
}
