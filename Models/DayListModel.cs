﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Tijdsregistratie.Models.DabaseModel;

namespace Tijdsregistratie.Models
{
    public class DayListModel : List<DayModel>
    {
        public DayListModel(IEnumerable<DayModel> collection) : base(collection)
        {
            LoggerControl.Logger.Trace($"new {GetType().FullName}.DayListModel()");
        }

        public static explicit operator DayListModel(timeregistrationDay[] values)
        {
            return new DayListModel(values.Select(value => (DayModel)value).ToList());
        }

        public DayModel GetOrCreate(DateTime date)
        {
            LoggerControl.Logger.Trace($"{GetType().FullName}.GetOrCreate({nameof(date)}={date:d})");
            DayModel day = Get(date);

            if (day is null)
            {
                day = new DayModel { Date = date };
                Add(day);
            }

            LoggerControl.Logger.Trace($"{GetType().FullName}.GetOrCreate() => {day}");
            return day;
        }

        public DayModel Get(DateTime date)
        {
            LoggerControl.Logger.Trace($"{GetType().FullName}.Get({nameof(date)}={date:d})");
            IEnumerable<DayModel> candidates = this.Where(day => day.Date == date.Date);

            switch (candidates.Count())
            {
                case 0:
                    LoggerControl.Logger.Trace($"{GetType().FullName}.Get() => null");
                    return null;
                case 1:
                    DayModel value = candidates.First();
                    LoggerControl.Logger.Trace($"{GetType().FullName}.Get() => {value}");
                    return value;
                default:
                    throw new Exception($"Fout in xml-bestand. Er zijn meer logdagen voor {date:d}.");
            }
        }

        internal void CreateLog(DateTime date, TimeOfDayModel timestamp, string description)
        {
            LoggerControl.Logger.Trace($"{GetType().FullName}.CreateLog({nameof(date)}={date:d}, {nameof(timestamp)}={timestamp}, {nameof(description)}=\"{description}\")");
            GetOrCreate(date).CreateLog(timestamp, description);
        }

        internal IEnumerable<string> GetRecentDescriptions()
        {
            LoggerControl.Logger.Trace($"{GetType().FullName}.GetRecentDescriptions()");
            Dictionary<string, double> candidates = new Dictionary<string, double>();

            for (DateTime day = DateTime.Now.Date.AddMonths(-1); day <= DateTime.Now; day = day.AddDays(1))
            {
                LogListModel logs = Get(day)?.Logs;

                if (logs is null)
                {
                    continue;
                }

                foreach (LogModel log in logs)
                {
                    string key = log.Description.Trim();

                    if (string.IsNullOrWhiteSpace(key))
                    {
                        continue;
                    }

                    double weight = 1d / (DateTime.Now - day - log.BeginTime).TotalHours;

                    if (candidates.ContainsKey(log.Description))
                    {
                        candidates[log.Description] += weight;
                    }
                    else
                    {
                        candidates.Add(log.Description, weight);
                    }
                }
            }

            LoggerControl.Logger.Trace($"{GetType().FullName}.GetRecentDescriptions() => count={candidates.Count()}");
            return candidates.OrderByDescending(candidate => candidate.Value).Take(10).Select(candidate => candidate.Key);
        }

        internal bool LastLogIsStopped(DateTime date)
        {
            LoggerControl.Logger.Trace($"{GetType().FullName}.LastLogIsStopped({nameof(date)}={date:d})");
            bool value = Get(date)?.LastLogIsStopped() ?? false;
            LoggerControl.Logger.Trace($"{GetType().FullName}.LastLogIsStopped() => {value}");
            return value;
        }

        internal LogModel GetNextLog(DateTime date, LogModel log)
        {
            LoggerControl.Logger.Trace($"{GetType().FullName}.GetNextLog({nameof(date)}={date:d}, {nameof(log)}={log})");
            LogModel value = Get(date)?.NextLog(log);
            LoggerControl.Logger.Trace($"{GetType().FullName}GetNextLog() => {value}");
            return value;
        }

        internal TimeSpan GetWeekPauze(DateTime date)
        {
            LoggerControl.Logger.Trace($"{GetType().FullName}.GetWeekPauze({nameof(date)}={date:d})");
            TimeSpan value = TimeSpan.FromTicks(DaysOfWeek(date).Select(day => day?.GetPauze().Ticks ?? 0L).Sum());
            LoggerControl.Logger.Trace($"{GetType().FullName}.GetWeekPauze() => {value}");
            return value;
        }

        internal void Delete(DateTime date, LogModel log)
        {
            LoggerControl.Logger.Trace($"{GetType().FullName}.Delete({nameof(date)}={date:d}, {nameof(log)}={log})");
            Get(date)?.Delete(log);
        }

        internal void DeleteStop(DateTime date, LogModel log)
        {
            LoggerControl.Logger.Trace($"{GetType().FullName}.DeleteStop({nameof(date)}={date:d}, {nameof(log)}={log})");
            Get(date)?.DeleteStop(log);
        }

        internal void UpdateLog(LogModel value, DateTime date, TimeOfDayModel timeOfDay, string description)
        {
            LoggerControl.Logger.Trace($"{GetType().FullName}.UpdateLog({nameof(value)}={value}, {nameof(date)}={date:d}, {nameof(timeOfDay)}={timeOfDay}, {nameof(description)}={description})");
            Get(date)?.UpdateLog(value, timeOfDay, description);
        }

        internal void Stop(DateTime date, TimeOfDayModel timeOfDay)
        {
            LoggerControl.Logger.Trace($"{GetType().FullName}.Stop({nameof(date)}={date:d}, {nameof(timeOfDay)}={timeOfDay})");
            Get(date)?.Stop(timeOfDay);
        }

        internal TimeSpan GetWeekTotal(DateTime date)
        {
            LoggerControl.Logger.Trace($"{GetType().FullName}.GetWeekTotal({nameof(date)}={date:d})");
            TimeSpan value = TimeSpan.FromTicks(DaysOfWeek(date).Select(day => day?.GetTotal().Ticks ?? 0L).Sum());
            LoggerControl.Logger.Trace($"{GetType().FullName}.GetWeekTotal({nameof(date)}={date:d}) => {value}");
            return value;
        }

        internal IEnumerable<DayModel> DaysOfWeek(DateTime date)
        {
            LoggerControl.Logger.Trace($"{GetType().FullName}.DaysOfWeek({nameof(date)}={date:d})");
            List<DayModel> results = new List<DayModel>();

            for (int weekday = 0; weekday < 7; weekday++)
            {
                DayModel result = Get(date.AddDays(weekday - (int)date.DayOfWeek));

                if (result != null)
                {
                    results.Add(Get(date.AddDays(weekday - (int)date.DayOfWeek)));
                }
            }

            LoggerControl.Logger.Trace($"{GetType().FullName}.DaysOfWeek() => count={results.Count()}");
            return results;
        }

        internal IEnumerable<LogModel> GetLogs(DateTime date)
        {
            LoggerControl.Logger.Trace($"{GetType().FullName}.GetLogs({nameof(date)}={date:d})");
            IEnumerable<LogModel> value = GetOrCreate(date)?.Logs ?? Enumerable.Empty<LogModel>();
            LoggerControl.Logger.Trace($"{GetType().FullName}.GetLogs() => {value}");
            return value;
        }

        internal void StartLoggingIfNotRunning()
        {
            LoggerControl.Logger.Trace($"{GetType().FullName}.StartLoggingIfNotRunning()");
            GetOrCreate(DateTime.Now.Date).StartTimeregistrationIfNotRunning();
        }

        internal TimeSpan GetDayTotal(DateTime date)
        {
            LoggerControl.Logger.Trace($"{GetType().FullName}.GetDayTotal({nameof(date)}={date:d})");
            TimeSpan value = GetOrCreate(date)?.GetTotal() ?? TimeSpan.Zero;
            LoggerControl.Logger.Trace($"{GetType().FullName}.GetDayTotal() => {value}");
            return value;
        }
    }
}
