﻿using System;
using System.Linq;

namespace Tijdsregistratie.Models.DabaseModel
{
    public partial class timeregistration
    {
        public static explicit operator timeregistration(TimeRegistrationModel value)
        {
            return new timeregistration
            {
                day = value.Days.Select(d => (timeregistrationDay)d).ToArray()
            };
        }
    }

    public partial class timeregistrationDay
    {
        public static explicit operator timeregistrationDay(DayModel value)
        {
            return new timeregistrationDay
            {
                date = value.Date,
                logging = value.Logs.Select(log => (timeregistrationDayLogging)log).ToArray()
            };
        }
    }

    public partial class timeregistrationDayLogging
    {
        public static explicit operator timeregistrationDayLogging(LogModel value)
        {
            return new timeregistrationDayLogging
            {
                description = value.Description,
                begin = default(DateTime).AddTicks(value.BeginTime.Ticks),
                endSpecified = value.EndTime != null,
                end = value.EndTime is null ? default : (DateTime)value.EndTime
            };
        }
    }
}