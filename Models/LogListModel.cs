﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Tijdsregistratie.Models.DabaseModel;

namespace Tijdsregistratie.Models
{
    public class LogListModel : List<LogModel>
    {
        public LogListModel() => LoggerControl.Logger.Trace($"new {GetType().FullName}.LogListModel()");

        public LogListModel(IEnumerable<LogModel> collection) : base(collection) => LoggerControl.Logger.Trace($"new {GetType().FullName}.LogListModel({nameof(collection)}.Count()={collection.Count()})");

        public static explicit operator LogListModel(timeregistrationDayLogging[] values)
        {
            return values == null ? new LogListModel() : new LogListModel(values.Select(value => (LogModel)value).ToList());
        }

        internal void Add(TimeOfDayModel timeOfDay, string description)
        {
            LoggerControl.Logger.Trace($"{GetType().FullName}.Add({nameof(timeOfDay)}={timeOfDay}, {nameof(description)}=\"{description}\")");

            Add(new LogModel
            {
                Description = description,
                BeginTime = timeOfDay,
                Duration = null
            });

            UpdateOthersAfterAdd();
        }

        private void UpdateOthersAfterAdd()
        {
            LoggerControl.Logger.Trace($"{GetType().FullName}.UpdateOthersAfterAdd()");
            TimeOfDayModel nextStart = null;

            foreach (LogModel log in this.OrderByDescending(log => log.BeginTime))
            {
                if (log.Duration is null && nextStart != null)
                {
                    log.Duration = (TimeSpan)(nextStart - log.BeginTime);
                }

                nextStart = log.BeginTime;
            }
        }

        internal void Delete(LogModel log)
        {
            LoggerControl.Logger.Trace($"{GetType().FullName}.Delete({nameof(log)}={log})");
            PrepareOthersBeforeRemove(log);
            Remove(log);
        }

        private void PrepareOthersBeforeRemove(LogModel log)
        {
            LoggerControl.Logger.Trace($"{GetType().FullName}.PrepareOthersBeforeRemove({nameof(log)}={log})");
            LogModel previous = Previous(log);

            if (previous?.EndTime != null)
            {
                if (previous.EndTime >= log.BeginTime)
                {
                    previous.EndTime = log.EndTime;
                }
            }
        }

        internal void UpdateLog(LogModel log, TimeOfDayModel timeOfDay, string description)
        {
            LoggerControl.Logger.Trace($"{GetType().FullName}.UpdateLog({nameof(log)}={log}, {nameof(timeOfDay)}={timeOfDay}, {nameof(description)}=\"{description}\")");
            PrepareOthersBeforeRemove(log);
            log.BeginTime = timeOfDay;
            log.Description = description;
            UpdateOthersAfterAdd();
        }

        public LogModel Previous(LogModel log)
        {
            LoggerControl.Logger.Trace($"{GetType().FullName}.Previous({nameof(log)}={log})");
            IOrderedEnumerable<LogModel> candidates = this.Where(l => l.BeginTime < log.BeginTime)?.OrderBy(l => l.BeginTime);
            LogModel value =candidates.Any() ? candidates.Last() : null;
            LoggerControl.Logger.Trace($"{GetType().FullName}.Previous() => {value}");
            return value;
        }

        internal void Stop(TimeOfDayModel timeOfDay)
        {
            LoggerControl.Logger.Trace($"{GetType().FullName}.Stop({nameof(timeOfDay)}={timeOfDay})");
            IEnumerable<LogModel> candidates = this.Where(log => log.BeginTime <= timeOfDay);

            if (candidates.Any())
            {
                LogModel logToModify = candidates.OrderBy(log => log.BeginTime).Last();
                logToModify.Duration = (TimeSpan)(timeOfDay - logToModify.BeginTime);

                if (logToModify.Duration <= TimeSpan.Zero)
                {
                    Remove(logToModify);

                    candidates = this.Where(log => log.BeginTime <= timeOfDay);

                    if (candidates.Any())
                    {
                        logToModify = candidates.OrderBy(log => log.BeginTime).Last();
                        logToModify.Duration = (TimeSpan)(timeOfDay - logToModify.BeginTime);
                    }
                }
            }
        }

        internal bool LastLogIsStopped()
        {
            LoggerControl.Logger.Trace($"{GetType().FullName}.LastLogIsStopped()");
            LogModel itemToStop = Current;
            bool value = itemToStop != null && itemToStop.Duration is null;
            LoggerControl.Logger.Trace($"{GetType().FullName}.LastLogIsStopped() => {value}");
            return value;
        }

        private LogModel Current
        {
            get
            {
                LoggerControl.Logger.Trace($"get {GetType().FullName}.Current()"); 
                IEnumerable<LogModel> candidates = this.Where(log => log.BeginTime < TimeOfDayModel.Now || (log.BeginTime == TimeOfDayModel.Now && log.EndTime is null));

                if (candidates is null || !candidates.Any())
                {
                    LoggerControl.Logger.Trace($"{GetType().FullName}.Current() => null");
                    return null;
                }

                LogModel value = candidates.OrderBy(log => log.BeginTime).First();
                LoggerControl.Logger.Trace($"{GetType().FullName}.Current() => {value}");
                return value;
            }
        }

        internal void StartTimeregistrationIfStopped()
        {
            LoggerControl.Logger.Trace($"get {GetType().FullName}.StartTimeregistrationIfStopped()");

            if (!this.Any())
            {
                Add(TimeOfDayModel.Now, "Diverse");
                return;
            }

            LogModel lastLog = this.OrderBy(l => l.BeginTime).Last();

            if (lastLog.EndTime != null)
            {
                Add(TimeOfDayModel.Now, lastLog.Description);
            }
        }

        public LogModel Next(LogModel log)
        {
            LoggerControl.Logger.Trace($"get {GetType().FullName}.Next({nameof(log)}={log})");
            IEnumerable<LogModel> candidates = this.Where(l => l.BeginTime > log.BeginTime);
            LogModel value = candidates.Any() ? candidates.OrderBy(l => l.BeginTime).First() : null;
            LoggerControl.Logger.Trace($"get {GetType().FullName}.Next() => {value}");
            return value;
        }

        internal void DeleteStop(LogModel log)
        {
            LoggerControl.Logger.Trace($"get {GetType().FullName}.DeleteStop({nameof(log)}={log})");
            LogModel next = Next(log);
            log.Duration = next == null ? (TimeSpan?)null : (TimeSpan)(next.BeginTime - log.BeginTime);
        }
    }
}
