﻿using System;

namespace Tijdsregistratie.Models
{
    public class TimeOfDayModel : IComparable
    {
        private long _ticks;

        public long Ticks
        {
            get => RoundUp(_ticks);
            set => _ticks = RoundUp(value);
        }

        public const long TicksPerMinute = TimeSpan.TicksPerMinute;
        public const long TicksPerHour = TimeSpan.TicksPerHour;
        public const long TicksPerDay = TimeSpan.TicksPerDay;

        public static readonly TimeOfDayModel Zero = (TimeOfDayModel)TimeSpan.Zero;

        /// <summary>
        /// Returns a TimeOfDayModel that represents a specified time, where the specification is in units of ticks.
        /// </summary>
        /// <param name="ticks">A number of ticks that represent a time.</param>
        /// <remarks>De waarde wordt afgerond naar de dichtsbijzijnde minuut.</remarks>
        public TimeOfDayModel(long ticks)
        {
            Ticks = ticks;
        }

        public static explicit operator TimeOfDayModel(TimeSpan v) { return new TimeOfDayModel(v.Ticks); }

        public static TimeOfDayModel FromMinutes(double value) => new TimeOfDayModel((long)(value * TicksPerMinute));

        public static TimeOfDayModel FromHours(double value) => new TimeOfDayModel((long)(value * TicksPerHour));

        public double TotalHours => (double)Ticks / (double)TicksPerHour;

        public static TimeOfDayModel Now => new TimeOfDayModel(DateTime.Now.TimeOfDay.Ticks);

        public int Minutes => (int)(Ticks / TicksPerMinute % 60);

        /// <summary>
        /// Geeft het huidige object terug in de vorm van een string.
        /// </summary>
        /// <returns>Het huidige object onder de vorm van een string.</returns>
        /// <remarks>Maak het formaat zelf en niet via een onderliggende functie TimeSpan.ToString("hh\\:mm") omdat die de de uren > 24 zou afkappen.</remarks>
        public override string ToString() => $"{(int)TotalHours}:{Minutes:00}";

        public string ToString(string format) => ((TimeSpan)this).ToString(format);

        public int CompareTo(object obj)
        {
            if (obj == null)
            {
                return 1;
            }

            if (obj is TimeOfDayModel other)
            {
                return Ticks.CompareTo(other.Ticks);
            }

            throw new ArgumentException($"Object is not a {GetType().FullName}.");
        }

        public static long RoundUp(long ticks)
        {
            ticks += TicksPerMinute / 2;
            return ticks - ticks % TicksPerMinute;
        }

        public static explicit operator DateTime(TimeOfDayModel v) => default(DateTime).AddTicks(v.Ticks);

        public static explicit operator TimeSpan(TimeOfDayModel v) => TimeSpan.FromTicks(v.Ticks);

        public static TimeOfDayModel operator +(TimeOfDayModel a, TimeSpan b) => new TimeOfDayModel(a.Ticks + b.Ticks);

        public static TimeOfDayModel operator -(TimeOfDayModel a, TimeOfDayModel b) => new TimeOfDayModel(a.Ticks - b.Ticks);

        public static TimeOfDayModel operator -(TimeSpan a, TimeOfDayModel b) => new TimeOfDayModel(a.Ticks - b.Ticks);

        public static TimeOfDayModel operator -(TimeOfDayModel a, TimeSpan b) => new TimeOfDayModel(a.Ticks - b.Ticks);

        public static bool operator >(TimeOfDayModel a, TimeOfDayModel b) => a.Ticks > b.Ticks;

        public static bool operator <(TimeOfDayModel a, TimeOfDayModel b) => a.Ticks < b.Ticks;

        public static bool operator >=(TimeOfDayModel a, TimeOfDayModel b) => a.Ticks >= b.Ticks;

        public static bool operator <=(TimeOfDayModel a, TimeOfDayModel b) => a.Ticks <= b.Ticks;
    }
}
