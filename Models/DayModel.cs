﻿using System;
using System.Linq;
using System.Reflection;
using Tijdsregistratie.Models.DabaseModel;

namespace Tijdsregistratie.Models
{
    public class DayModel
    {
        private DateTime _date = DateTime.Now.Date;
        private readonly LogListModel _logs = new LogListModel();

        public DateTime Date
        {
            get => _date.Date;
            set => _date = value.Date;
        }

        public LogListModel Logs
        {
            get => _logs;
            set
            {
                _logs.Clear();
                _logs.AddRange(value);
            }
        }

        public DayModel() => LoggerControl.Logger.Trace($"new {GetType().FullName}.DayModel()");

        public TimeSpan GetTotal()
        {
            LoggerControl.Logger.Trace($"{GetType().FullName}.GetTotal()");
            long totalTicks = 0L;

            if (_logs != null && _logs.Any())
            {
                LogModel next = null;

                foreach (LogModel log in _logs.OrderByDescending(log => log.BeginTime))
                {
                    if (next is null)
                    {
                        if (log.Duration is null)
                        {
                            if (_date == DateTime.Now.Date)
                            {
                                totalTicks += TimeOfDayModel.Now.Ticks - log.BeginTime.Ticks;
                            }
                            else
                            {
                                totalTicks += TimeOfDayModel.TicksPerDay - log.BeginTime.Ticks;
                            }
                        }
                        else
                        {
                            totalTicks += log.Duration.Value.Ticks;
                        }
                    }
                    else
                    {
                        if (log.EndTime is null || log.EndTime > next.BeginTime)
                        {
                            totalTicks += next.BeginTime.Ticks - log.BeginTime.Ticks;
                        }
                        else
                        {
                            totalTicks += log.Duration.Value.Ticks;
                        }
                    }

                    next = log;
                }
            }

            TimeSpan value = TimeSpan.FromTicks(totalTicks);
            LoggerControl.Logger.Trace($"{GetType().FullName}.GetTotal() => {value}");
            return value;
        }

        public TimeSpan GetPauze()
        {
            LoggerControl.Logger.Trace($"{GetType().FullName}.GetPauze()");
            long value = 0;
            LogModel previousLog = null;

            foreach (LogModel log in _logs.OrderBy(log => log.BeginTime))
            {
                if (previousLog?.EndTime != null && log.BeginTime.Ticks > previousLog.EndTime.Ticks)
                {
                    value += log.BeginTime.Ticks - previousLog.EndTime.Ticks;
                }

                previousLog = log;
            }

            TimeSpan ticks = TimeSpan.FromTicks(value);
            LoggerControl.Logger.Trace($"{GetType().FullName}.GetPauze() => {ticks}");
            return ticks;
        }

        public static explicit operator DayModel(timeregistrationDay value)
        {
            return new DayModel
            {
                Date = value.date,
                Logs = (LogListModel)value.logging
            };
        }

        internal void CreateLog(TimeOfDayModel timeOfDay, string description)
        {
            LoggerControl.Logger.Trace($"{GetType().FullName}.CreateLog({nameof(timeOfDay)}={timeOfDay}, {nameof(description)}=\"{description}\")");

            if (Logs is null)
            {
                Logs = new LogListModel();
            }

            Logs.Add(timeOfDay, description);
        }

        internal void Delete(LogModel log)
        {
            LoggerControl.Logger.Trace($"{GetType().FullName}.Delete({nameof(log)}={log})");
            _logs.Delete(log);
        }

        internal void StartTimeregistrationIfNotRunning()
        {
            LoggerControl.Logger.Trace($"{GetType().FullName}.StartTimeregistrationIfNotRunning()");
            if (Logs is null)
            {
                Logs = new LogListModel();
            }

            Logs.StartTimeregistrationIfStopped();
        }

        internal void UpdateLog(LogModel value, TimeOfDayModel timeOfDay, string description)
        {
            LoggerControl.Logger.Trace($"{GetType().FullName}.UpdateLog({nameof(value)}={value}, {nameof(timeOfDay)}={timeOfDay}, {nameof(description)}=\"{description}\")");
            _logs.UpdateLog(value, timeOfDay, description);
        }

        internal void Stop(TimeOfDayModel timeOfDay)
        {
            LoggerControl.Logger.Trace($"{GetType().FullName}.Stop({nameof(timeOfDay)}={timeOfDay})");
            _logs.Stop(timeOfDay);
        }

        internal void DeleteStop(LogModel log)
        {
            LoggerControl.Logger.Trace($"{GetType().FullName}.DeleteStop({nameof(log)}={log})");
            _logs.DeleteStop(log);
        }

        internal bool LastLogIsStopped()
        {
            LoggerControl.Logger.Trace($"{GetType().FullName}.LastLogIsStopped()");
            bool value =_logs.LastLogIsStopped();
            LoggerControl.Logger.Trace($"{GetType().FullName}.LastLogIsStopped() => {value}");
            return value;
        }

        internal LogModel NextLog(LogModel log)
        {
            LoggerControl.Logger.Trace($"{GetType().FullName}.NextLog({nameof(log)}={log})");
            LogModel value = Logs?.Next(log);
            LoggerControl.Logger.Trace($"{GetType().FullName}.NextLog() => {value}");
            return value;
        }
    }
}
