﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Tijdsregistratie.Models.DabaseModel;

namespace Tijdsregistratie.Models
{
    public class TimeRegistrationModel
    {
        public DayListModel Days;

        public static explicit operator TimeRegistrationModel(timeregistration value)
        {
            return new TimeRegistrationModel
            {
                Days = (DayListModel)value.day
            };
        }

        public TimeRegistrationModel() => LoggerControl.Logger.Trace($"new {GetType().FullName}.TimeRegistrationModel()");

        internal void CreateLog(DateTime date, TimeOfDayModel timestamp, string description)
        {
            LoggerControl.Logger.Info($"{GetType().FullName}.CreateLog({nameof(date)}={date:d}, {nameof(timestamp)}={timestamp}, {nameof(description)}=\"{description}\")");
            Days.CreateLog(date, timestamp, description);
        }

        internal void DeleteStop(DateTime date, LogModel log)
        {
            LoggerControl.Logger.Info($"{GetType().FullName}.DeleteStop({nameof(date)}={date:d}, {nameof(log)}={log})");
            Days.DeleteStop(date, log);
        }

        internal void Stop(DateTime date, TimeOfDayModel timeOfDay)
        {
            LoggerControl.Logger.Info($"{GetType().FullName}.Stop({nameof(date)}={date:d}, {nameof(timeOfDay)}={timeOfDay})");
            Days.Stop(date, timeOfDay);
        }

        internal void Stop()
        {
            LoggerControl.Logger.Info($"{GetType().FullName}.Stop()");
            Stop(DateTime.Now.Date, TimeOfDayModel.Now);
        }

        internal void Delete(DateTime date, LogModel log)
        {
            LoggerControl.Logger.Info($"{GetType().FullName}.Delete({nameof(date)}={date:d}, {nameof(log)}={log})");
            Days.Delete(date, log);
        }

        internal void UpdateLog(LogModel value, DateTime date, TimeOfDayModel timeOfDay, string description)
        {
            LoggerControl.Logger.Info($"{GetType().FullName}.UpdateLog({nameof(value)}={value}, {nameof(date)}={date:d}, {nameof(timeOfDay)}={timeOfDay}, {nameof(description)}=\"{description}\")");
            Days.UpdateLog(value, date, timeOfDay, description);
        }

        internal IEnumerable<string> RecentDescriptions
        {
            get
            {
                LoggerControl.Logger.Trace($"get {GetType().FullName}.RecentDescriptions");
                return Days.GetRecentDescriptions();
            }
        }

        internal void StartLoggingIfNotRunning()
        {
            LoggerControl.Logger.Info($"{GetType().FullName}.StartLoggingIfNotRunning()");
            Days.StartLoggingIfNotRunning();
        }

        internal TimeSpan GetWeekTotal(DateTime date)
        {
            LoggerControl.Logger.Trace($"{GetType().FullName}.GetWeekTotal({nameof(date)}={date:d})");
            TimeSpan value = Days?.GetWeekTotal(date) ?? TimeSpan.Zero;
            LoggerControl.Logger.Trace($"{GetType().FullName}.GetWeekTotal() => {value}");
            return value;
        }

        internal TimeSpan GetWeekPauze(DateTime date)
        {
            LoggerControl.Logger.Trace($"{GetType().FullName}.GetWeekPauze({nameof(date)}={date:d})");
            TimeSpan value = Days?.GetWeekPauze(date) ?? TimeSpan.Zero;
            LoggerControl.Logger.Trace($"{GetType().FullName}.GetWeekPauze() => {value}");
            return value;
        }

        internal LogModel GetNextLog(DateTime date, LogModel log)
        {
            LoggerControl.Logger.Trace($"{GetType().FullName}.GetNextLog({nameof(date)}={date:d}, {nameof(log)}={log})");
            LogModel value = Days?.GetNextLog(date, log);
            LoggerControl.Logger.Trace($"{GetType().FullName}.GetNextLog() => {value}");
            return value;
        }

        internal bool LastLogIsStopped(DateTime date)
        {
            LoggerControl.Logger.Trace($"{GetType().FullName}.LastLogIsStopped({nameof(date)}={date:d})");
            bool value = Days?.LastLogIsStopped(date) ?? false;
            LoggerControl.Logger.Trace($"{GetType().FullName}.LastLogIsStopped() => {value}");
            return value;
        }

        internal IEnumerable<LogModel> GetDayLogs(DateTime date)
        {
            LoggerControl.Logger.Trace($"{GetType().FullName}.GetDayLogs({nameof(date)}={date:d})");
            IEnumerable<LogModel> value = Days?.GetLogs(date) ?? Enumerable.Empty<LogModel>();
            LoggerControl.Logger.Trace($"{GetType().FullName}.GetDayLogs() => count={value.Count()}");
            return value;
        }

        internal TimeSpan GetDayTotal(DateTime date)
        {
            LoggerControl.Logger.Trace($"{GetType().FullName}.GetDayTotal({nameof(date)}={date:d})");
            TimeSpan value = Days?.GetDayTotal(date) ?? TimeSpan.Zero;
            LoggerControl.Logger.Trace($"{GetType().FullName}.GetDayTotal() => {value}");
            return value;
        }
    }
}
